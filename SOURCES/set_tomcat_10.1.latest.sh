#!/bin/bash
#set -x
v=`ls -1d 10.1.? 10.1.?? 2>/dev/null|cut -d "." -f3 |sort -n |tail -1`
i=`ls -l 10.1.latest.bak* 2>/dev/null|wc -l`
test -L 10.1.latest && mv 10.1.latest 10.1.latest.bak.${i}
rc=`ln -fs 10.1.${v} 10.1.latest`
exit $rc

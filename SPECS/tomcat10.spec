%define version		10.1.7
%define vers		1017
%define prefix		/var/lib/tomcat
%global tcuid 53
%global homedir %{_datadir}/%{name}

Name:           	tomcat
Summary:        	Apache Tomcat V%{version}
License:        	Apache-2.0
Group:          	Productivity/Networking/Web/Servers
Version:        	%{version}
Release:        	1.2
Source0:        	http://mirror.arcor-online.net/www.apache.org/tomcat/tomcat-9/v%{version}/bin/apache-tomcat-%{version}.tar.gz
#Source1:               fixserverproperties.sh
Source1:                ServerInfo.properties
Source2:        	http://mirror.softaculous.com/apache/tomcat/tomcat-9/v%{version}/bin/extras/catalina-jmx-remote.jar
Source3:                set_tomcat_10.1.latest.sh
SOurce4:		tomcat.service
Source99:        	https://www.apache.org/dist/tomcat/tomcat-9/v%{version}/bin/apache-tomcat-%{version}.tar.gz.md5
Url:            	http://tomcat.apache.org/
AutoReqProv:    	on
BuildArch:              noarch
BuildRoot:      	%{_tmppath}/%{name}-%{version}-build

#!BuildIgnore:  nspr dba-geoip-148 dba-geoip-160 dba-git-2191

# needed for brp-check-bytecode-version (jar, fastjar would do as well)  
BuildRequires:          unzip

%if 0%{?suse_version} >= 1100
BuildRequires:          -post-build-checks
%endif

#!BuildIgnore:  dba-openssl-098o dba-openssl-098r dba-openssl-098x dba-openssl-101c dba-openssl-101e dba-openssl-101g-static


%description
Apache Tomcat is an open source software implementation of the Java Servlet and JavaServer Pages technologies. The Java Servlet and JavaServer Pages specifications are developed under the Java Community Process.

Apache Tomcat is developed in an open and participatory environment and released under the Apache License version 2. Apache Tomcat is intended to be a collaboration of the best-of-breed developers from around the world. We invite you to participate in this open development project. To learn more about getting involved, click here.

Apache Tomcat powers numerous large-scale, mission-critical web applications across a diverse range of industries and organizations. Some of these users and their stories are listed on the PoweredBy wiki page.

Apache Tomcat, Tomcat, Apache, the Apache feather, and the Apache Tomcat project logo are trademarks of the Apache Software Foundation.


%prep
#  
#  O/ ._ .__ ._
#  /O |_)|(/_|_)
#     |      |
#
md5sum %{SOURCE0} | awk '{print $1;}' | cmp - %{SOURCE99}

%setup -q -n apache-tomcat-%{version}

%build
#
#  O/ |_    o| _|
#  /O |_)|_|||(_|
#
echo "no build process needed" 
 
%install
#
#  O/ o._  __|_ _.||
#  /O || |_> |_(_|||
#  
#
#

# suppress bytecode version error
%if 0%{?suse_version} >= 1100
export NO_BRP_CHECK_BYTECODE_VERSION=true
%endif

%{__mkdir_p} %{buildroot}%{prefix}
for D in bin conf lib logs temp webapps work
do
  # install -m 644 -D $D %{buildroot}%{_sysconfdir}/xinetd.d/apgd
  cp -a $D %{buildroot}%{prefix}
done
%{__install} -d -m 0755 ${RPM_BUILD_ROOT}%{_unitdir}
chmod 644 %{buildroot}%{prefix}/conf/*
install -m 644 %{S:2} %{buildroot}%{prefix}/lib/
%{__install} -m 0755 %{S:3} %{buildroot}%{prefix}/bin/set_tomcat_10.1.latest.sh

find %{buildroot}%{prefix} -type f -name "safeToDelete.tmp" -exec rm -vf {} \;

%{__mkdir_p} %{buildroot}%{prefix}/lib/org/apache/catalina/util/
%{__install} -m 0755 %{S:1}  %{buildroot}%{prefix}/lib/org/apache/catalina/util/ServerInfo.properties
	
%{__install} -m 0644 %{SOURCE4} \
    ${RPM_BUILD_ROOT}%{_unitdir}/%{name}.service

%pre
# add the tomcat user and group
getent group tomcat >/dev/null || %{_sbindir}/groupadd -f -g %{tcuid} -r tomcat
if ! getent passwd tomcat >/dev/null ; then
    if ! getent passwd %{tcuid} >/dev/null ; then
        %{_sbindir}/useradd -r -u %{tcuid} -g tomcat -d %{homedir} -s /sbin/nologin -c "Apache Tomcat" tomcat
        # Tomcat uses a reserved ID, so there should never be an else
    fi
fi
exit 0


%post
cd %{prefix}/..
%{prefix}/bin/set_tomcat_10.1.latest.sh
%systemd_post %{name}.service


%files
#       _
#  O/ _|_o| _  _
#  /O  | ||(/__>
#
%defattr(0664,tomcat,root,0775)
%dir %{prefix} 
%attr(0755,root,tomcat)%{prefix}/bin
%{prefix}/conf
%{prefix}/lib
%{prefix}/logs
%{prefix}/temp
%{prefix}/webapps
%{prefix}/work
%attr(0644,root,root) %{_unitdir}/%{name}.service


%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}


%changelog
* Wed Mar 29 2023 Philipp Hügelmeyer <philipp.huegelmeyer@ble.de>
- Updated to 10.1.7
- Fixed Service
- Changed from root to tomcat user

* Tue Mar 28 2023 Philipp Hügelmeyer <philipp.huegelmeyer@ble.de>
- Added service

* Wed Mar 01 2023 Sebastian Roth <sebastian.roth@bertelsmann.de>
- inital version
